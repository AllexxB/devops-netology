# devops-netology
**/.terraform/* - все файлы в директориях, которые заканчиваются /.terraform
*.tfstate -  все файлы с раширением .tfstate
*.tfstate.* - все файлы, которые содержат в названии .tfstate.
crash.log - конкретный файл
crash.*.log -  файлы, которые начинаются на crash и с расширением .log
*.tfvars - все файлы с расширение .tfvars
*.tfvars.json - все файлы, которые заканчиваются на .tfvars.json
override.tf override.tf.json - конкретные файлы
*_override.tf *_override.tf.json -  файлы которые заканчиваются соответсвенно на _override.tf и _override.tf.json
.terraformrc terraform.rc - конкретные файлы

